# azure

## Azure Setup

```
1. brew update && brew install azure-cli
2. az login
```

## Terraform Installation

```
1. brew tap hashicorp/tap
2. brew install hashicorp/tap/terraform
3. brew upgrade hashicorp/tap/terraform
```

## Terraform State Persistence
Run this script for setting up Azure Storage Blob for persisting
Terraform state.
```
./tf_state.sh
```
Note the storage account name - `STORAGE_ACCOUNT_NAME` - from the output of `tf_state.sh`.
This is one time step only. Once created, simply update terraform configuration
in `dev/01_vnet.tf` as shown below:
```
terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = ">= 2.26"
    }
  }
  backend "azurerm" {
    resource_group_name   = "tfstate"
    storage_account_name  = "tfstate12345"
    container_name        = "tstate"
    key                   = "terraform.tfstate"
  }
}
```

### Steps for run Terraform module

```
1. terraform init
2. terraform plan
3. terraform apply
```
Note: Before running `terraform apply` make sure there an ssh key exists in `~/.ssh/id_rsa`.
The key name can be changed but needs update to linux_vm module parameters wherever it's used.

### Jenkins Setup

1. Install recommended plugins
2. Install docker pipeline plugin
3. Locally run `05-docker-registry.tf` to acquire service principal username & password
4. Create credentials in Jenkins for service principal username & password in step 3


### Jenkins New Job Setup

1. Create SSH Key for repo to be built on Jenkins
2. Add the private key in Jenkins as a new credential
3. Add the public key in Git repo as Deploy Key
4. Add JenkinsFile to repository - similar to `azure` repo
5. Clone existing `azure` job
6. Use `Configure` option in the new job
7. Change Pipeline SCM repo URL & credentials - use the credentials created in step 2


### ACR Kubernetes Secret

```
kubectl create secret docker-registry <secret-name> \
    --namespace <namespace> \
    --docker-server=<container-registry-name>.azurecr.io \
    --docker-username=<service-principal-ID> \
    --docker-password=<service-principal-password>
```
Use service principal uid & password created using `05-docker-registry.tf` in the command above.
