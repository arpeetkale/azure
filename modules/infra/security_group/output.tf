output "bastion_application_security_group_id" {
  value       = azurerm_application_security_group.bastion_asg.id
  description = "Bastion application security group id"
}

output "bastion_network_security_group_id" {
  value       = azurerm_network_security_group.bastion_nsg.id
  description = "Bastion network security group id"
}

output "jenkins_network_security_group_id" {
  value       = azurerm_network_security_group.jenkins_nsg.id
  description = "Jenkins network security group id"
}
