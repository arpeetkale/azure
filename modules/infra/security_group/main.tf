# Acquire Resource Group Metadata
data "azurerm_resource_group" "rg" {
  name = var.resource_group_name
}

# Create Application Security Group for Bastion Host
resource "azurerm_application_security_group" "bastion_asg" {
  name                = "${var.infra_prefix}-${var.environment}-BASTION-ASG"
  location            = data.azurerm_resource_group.rg.location
  resource_group_name = data.azurerm_resource_group.rg.name
  tags = var.tags
}

# Create Network Security Group for Bastion Host
resource "azurerm_network_security_group" "bastion_nsg" {
  name                = "${var.infra_prefix}-${var.environment}-BASTION-NSG"
  location            = data.azurerm_resource_group.rg.location
  resource_group_name = data.azurerm_resource_group.rg.name
  tags                = var.tags
}

# Create Network Security Group for Jenkins Host
resource "azurerm_network_security_group" "jenkins_nsg" {
  name                = "${var.infra_prefix}-${var.environment}-JENKINS-NSG"
  location            = data.azurerm_resource_group.rg.location
  resource_group_name = data.azurerm_resource_group.rg.name
  tags                = var.tags
}

resource "azurerm_network_security_rule" "bastion_ssh_inbound" {
  name                        = "${var.infra_prefix}-${var.environment}-BASTION-SSH-INBOUND"
  priority                    = 102
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = data.azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.bastion_nsg.name
}

resource "azurerm_network_security_rule" "bastion_ssh_outbound" {
  name                        = "${var.infra_prefix}-${var.environment}-BASTION-SSH-OUTBOUND"
  priority                    = 103
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = "*"
  destination_application_security_group_ids = [ azurerm_application_security_group.bastion_asg.id ]
  resource_group_name         = data.azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.bastion_nsg.name
  depends_on                  = [azurerm_application_security_group.bastion_asg]
}

resource "azurerm_network_security_rule" "bastion_deny_all_outbound" {
  name                        = "${var.infra_prefix}-${var.environment}-BASTION-DENY-OUTBOUND"
  priority                    = 104
  direction                   = "Outbound"
  access                      = "Deny"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = data.azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.bastion_nsg.name
}

resource "azurerm_network_security_rule" "jenkins_ssh_inbound" {
  name                        = "${var.infra_prefix}-${var.environment}-JENKINS-SSH-INBOUND"
  priority                    = 105
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "22"
  destination_address_prefix  = "*"
  source_application_security_group_ids = [azurerm_application_security_group.bastion_asg.id]
  resource_group_name         = data.azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.jenkins_nsg.name
}

resource "azurerm_network_security_rule" "jenkins_http_inbound" {
  name                        = "${var.infra_prefix}-${var.environment}-JENKINS-HTTP-INBOUND"
  priority                    = 106
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  source_address_prefix       = "Internet"
  destination_port_range      = "8080"
  destination_address_prefix  = "*"
  resource_group_name         = data.azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.jenkins_nsg.name
}
