variable "resource_group_name" {}

variable "infra_prefix" {}

variable "environment" {}

variable "tags" {}
