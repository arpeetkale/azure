# Acquire Resource Group Metadata
data "azurerm_resource_group" "rg" {
  name = var.resource_group_name
}

# Acquire Subnet Metadata
data "azurerm_subnet" "subnet" {
  resource_group_name  = data.azurerm_resource_group.rg.name
  virtual_network_name = var.vnet_name
  name                 = var.subnet_name
}

# Create Public IP for Linux VM
resource "azurerm_public_ip" "linux_vm_public_ip" {
  count               = "${var.linux_vm_create_public_ip ? 1 : 0}"
  name                = "${var.linux_vm_name}-PIP"
  location            = data.azurerm_resource_group.rg.location
  resource_group_name = data.azurerm_resource_group.rg.name
  allocation_method   = "Dynamic"
  tags                = var.tags
}

# Create Network Interface for the Linux VM
resource "azurerm_network_interface" "linux_vm_nic" {
  name                = "${var.linux_vm_name}-NIC"
  location            = data.azurerm_resource_group.rg.location
  resource_group_name = data.azurerm_resource_group.rg.name
  
  ip_configuration {
    name                          = "${var.linux_vm_name}-NIC-IP-CFG"
    subnet_id                     = data.azurerm_subnet.subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = length(azurerm_public_ip.linux_vm_public_ip) > 0 ? azurerm_public_ip.linux_vm_public_ip[0].id : null
  }
}

# Associate Network Security Group to Network Interface
resource "azurerm_network_interface_security_group_association" "linux_vm_nsg_nic_association" {
  network_interface_id      = azurerm_network_interface.linux_vm_nic.id
  network_security_group_id = var.linux_vm_network_security_group_id
}

resource "azurerm_network_interface_application_security_group_association" "linux_vm_asg_nic_association" {
  network_interface_id          = azurerm_network_interface.linux_vm_nic.id
  application_security_group_id = var.linux_vm_application_security_group_id
}

# Create Linux VM
resource "azurerm_linux_virtual_machine" "linux_vm" {
  name                = var.linux_vm_name
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = data.azurerm_resource_group.rg.location
  size                = var.linux_vm_size
  admin_username      = var.linux_vm_admin_username
  network_interface_ids = [azurerm_network_interface.linux_vm_nic.id]
  disable_password_authentication = true

  admin_ssh_key {
    username   = var.linux_vm_admin_username
    public_key = file(var.linux_vm_ssh_key)
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = var.linux_vm_storage_account_type
  }

  source_image_reference {
    publisher = var.linux_vm_ami_publisher
    offer     = var.linux_vm_ami_offer
    sku       = var.linux_vm_ami_sku
    version   = var.linux_vm_ami_sku_version
  }
}

# Create Linux VM Extension to run post-creation script
resource "azurerm_virtual_machine_extension" "linux_vm_extension" {
  count                 = "${var.linux_vm_extension_script_file != "" ? 1 : 0}"
  name                  = "${var.linux_vm_name}-EXTENSION"
  virtual_machine_id  = azurerm_linux_virtual_machine.linux_vm.id
  type_handler_version  = "2.1"
  type                  = "CustomScript"
  publisher             = "Microsoft.Azure.Extensions"
  tags                  = var.tags
  protected_settings    = <<PROT
  {
    "script": "${base64encode(file(var.linux_vm_extension_script_file))}"
  }
  PROT
}
