output "linux_vm_id" {
  value       = azurerm_linux_virtual_machine.linux_vm.id
  description = "Id of the Linux VM created"
}

output "linux_vm_public_ip" {
  value       = azurerm_linux_virtual_machine.linux_vm.public_ip_address
  description = "Public IP of the Linux VM created"
}

output "linux_vm_private_ip" {
  value       = azurerm_linux_virtual_machine.linux_vm.private_ip_address
  description = "Private IP of the Linux VM created"
}

output "linux_vm_network_interface_id" {
  value       = azurerm_network_interface.linux_vm_nic.id
  description = "NIC of the Linux VM created"
}

