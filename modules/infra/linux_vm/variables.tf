variable "resource_group_name" {}

variable "vnet_name" {}

variable "subnet_name" {}

variable "linux_vm_name" {}

variable "linux_vm_network_security_group_id" {}

variable "linux_vm_application_security_group_id" {}

variable "linux_vm_extension_script_file" {
  type        = string
  default     = ""
  description = "Script file to run post VM creation"
}

variable "linux_vm_create_public_ip" {
  default     = false
  description = "Whether to create public ip for the vm or not"
}

variable "linux_vm_size" {
  type        = string
  default     = "Standard_F2"
  description = "Size of the Linux VM"
}

variable "linux_vm_admin_username" {
  type        = string
  default     = "adminuser"
  description = "Linux VM Admin Username"
}

variable "linux_vm_ssh_key" {
  type        = string
  default     = "~/.ssh/id_rsa.pub"
  description = "Path to ssh key to user for Linux VM"
}

variable "linux_vm_ami_publisher" {
  type    = string
  default = "Canonical"
  description = "The publisher of the machine image (AMI) to use."
}

variable "linux_vm_ami_offer" {
  type    = string
  default = "UbuntuServer"
  description = "The offer (ex. UbuntuServer) of the machine image (AMI) to use."
}

variable "linux_vm_ami_sku" {
  type    = string
  default = "18.04-LTS"
  description = "The sku of the machine image (AMI) to use."
}

variable "linux_vm_ami_sku_version" {
  type    = string
  default = "latest"
  description = "The version of the machine image (AMI) to use."
}

variable "linux_vm_storage_account_type" {
  type    = string
  default = "Standard_LRS"
  description = "The storage account type of the machine image (AMI) to use."
}

variable "tags" {}
