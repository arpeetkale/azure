output "acr_id" {
    value = azurerm_container_registry.acr.id
    description = "Id of azure container registry"
}

output "acr_login_server" {
    value = azurerm_container_registry.acr.login_server
    description = "Login server of azure container registry"
}
