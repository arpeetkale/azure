variable "infra_prefix" {}

variable "environment" {}

variable "tags" {}

variable "resource_group_name" {}

variable "location" {}
