# TODO: This file could be abstracted as a module to create ACR registry & a service principal to access/use it.

# Create a new container registry - ACR
resource "azurerm_container_registry" "acr" {
  name                     = "${var.infra_prefix}${var.environment}REGISTRY"
  resource_group_name      = var.resource_group_name
  location                 = var.location
  sku                      = "Standard"
  admin_enabled            = false
  tags                     = var.tags
}
