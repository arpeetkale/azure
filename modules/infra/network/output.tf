output "resource_group_name" {
  value       = azurerm_resource_group.rg.name
  description = "Name of the Resource Group created"
}

output "location" {
  value       = azurerm_resource_group.rg.location
  description = "Location of the Resource Group created"
}

output "virtual_network_name" {
  value       = azurerm_virtual_network.vnet.name
  description = "Name of the Virtual Network created"
}

output "public_subnet_1_name" {
  value       = azurerm_subnet.public_subnet_1.name
  description = "Name of the public subnet 1 created"
}

output "private_subnet_1_name" {
  value       = azurerm_subnet.private_subnet_1.name
  description = "Name of the private subnet 1 created"
}

output "public_subnet_1_id" {
  value       = azurerm_subnet.public_subnet_1.id
  description = "Id of the public subnet 1 created"
}

output "private_subnet_1_id" {
  value       = azurerm_subnet.private_subnet_1.id
  description = "Id of the private subnet 1 created"
}

output "infra_prefix" {
  value       = var.infra_prefix
  description = "Prefix for infra resources"
}

output "environment" {
  value       = var.environment
  description = "Environment name"
}
