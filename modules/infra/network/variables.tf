variable "location" {}

variable "vnet_cidr" {}

variable "public_subnet_1_cidr" {}

variable "private_subnet_1_cidr" {}

variable "infra_prefix" {}

variable "environment" {}

variable "tags" {}
