# Create Azure Resource Group
resource "azurerm_resource_group" "rg" {
  name     = "${var.infra_prefix}-${var.environment}-RG"
  location = var.location
  tags = var.tags
}

# Create Azure Virtual Network
resource "azurerm_virtual_network" "vnet" {
  name                = "${var.infra_prefix}-${var.environment}-VNET"
  address_space       = [var.vnet_cidr]
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  tags = var.tags
}

# Create Public Subnet 1 in Virtual Network
resource "azurerm_subnet" "public_subnet_1" {
  name                 = "${var.infra_prefix}-${var.environment}-PUB-SUBNET-1"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = [var.public_subnet_1_cidr]
}

# Create Private Subnet 1 in Virtual Network
resource "azurerm_subnet" "private_subnet_1" {
  name                 = "${var.infra_prefix}-${var.environment}-PRV-SUBNET-1"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = [var.private_subnet_1_cidr]
}

# Create Public IP for NAT Gateway for Subnet 1
resource "azurerm_public_ip" "public_subnet_1_nat_gateway_public_ip" {
  name                = "${var.infra_prefix}-${var.environment}-PUB-SUBNET-1-NAT-PIP"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
  sku                 = "Standard"
  zones               = ["1"]
  tags = var.tags
}

# Create NAT Gateway for Public Subnet 1
resource "azurerm_nat_gateway" "public_subnet_1_nat_gateway" {
  name                    = "${var.infra_prefix}-${var.environment}-PUB-SUBNET-1-NAT"
  location                = azurerm_resource_group.rg.location
  resource_group_name     = azurerm_resource_group.rg.name
  sku_name                = "Standard"
  zones                   = ["1"]
  tags = var.tags
}

# Associate Public IP with NAT Gateway for Public Subnet 1
resource "azurerm_nat_gateway_public_ip_association" "public_ip_nateway_subnet_1_association" {
  nat_gateway_id       = azurerm_nat_gateway.public_subnet_1_nat_gateway.id
  public_ip_address_id = azurerm_public_ip.public_subnet_1_nat_gateway_public_ip.id
}

# Create Public IP for NAT Gateway for Private Subnet 1
resource "azurerm_public_ip" "private_subnet_1_nat_gateway_public_ip" {
  name                = "${var.infra_prefix}-${var.environment}-PRV-SUBNET-1-NAT-PIP"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
  sku                 = "Standard"
  zones               = ["1"]
  tags = var.tags
}

# Create NAT Gateway for Private Subnet 1
resource "azurerm_nat_gateway" "private_subnet_1_nat_gateway" {
  name                    = "${var.infra_prefix}-${var.environment}-PRV-SUBNET-1-NAT"
  location                = azurerm_resource_group.rg.location
  resource_group_name     = azurerm_resource_group.rg.name
  sku_name                = "Standard"
  zones                   = ["1"]
  tags = var.tags
}

# Associate Public IP with NAT Gateway for Private Subnet 1
resource "azurerm_nat_gateway_public_ip_association" "public_ip_nateway_subnet_2_association" {
  nat_gateway_id       = azurerm_nat_gateway.private_subnet_1_nat_gateway.id
  public_ip_address_id = azurerm_public_ip.private_subnet_1_nat_gateway_public_ip.id
}
