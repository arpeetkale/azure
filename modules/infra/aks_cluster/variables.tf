variable "infra_prefix" {}

variable "environment" {}

variable "tags" {}

variable "resource_group_name" {}

variable "location" {}

variable "admin_user_for_cluster_vm" {
  type = string
  default = "adminuser"
  description = "Admin user for AKS cluster nodepool VM"
}

variable "cluster_vm_ssh_key" {
  type        = string
  default     = "~/.ssh/id_rsa.pub"
  description = "Path to ssh key to user for Linux VM"
}

variable "cluster_node_pool_size" {
  type = number
  default = 1
  description = "Size of cluster node pool"
}

variable "cluster_node_vm_size" {
  type = string
  default = "Standard_D2_v2"
  description = "Size of cluster node pool vm"
}
