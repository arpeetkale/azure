output "cluster_id" {
  value = azurerm_kubernetes_cluster.k8s.id
  description = "Cluster ID"
}

output "cluster_fqdn" {
  value = azurerm_kubernetes_cluster.k8s.fqdn
  description = "Cluster FQDN"
}

output "cluster_name" {
  value = azurerm_kubernetes_cluster.k8s.name
  description = "Name of K8's cluster"
}

output "cluster_node_pool_name" {
  value = azurerm_kubernetes_cluster.k8s.name
  description = "Name of K8's cluster"
}

output "cluster_node_resource_group" {
  value = azurerm_kubernetes_cluster.k8s.node_resource_group
  description = "Cluster Node Resource Group"
}

output "cluster_client_key" {
  value = azurerm_kubernetes_cluster.k8s.kube_config[0].client_key
  description = "Cluster Client Key"
}

output "cluster_client_certificate" {
  value = azurerm_kubernetes_cluster.k8s.kube_config[0].client_certificate
  description = "Cluster Client Certificate"
}

output "cluster_ca_certificate" {
  value = azurerm_kubernetes_cluster.k8s.kube_config[0].cluster_ca_certificate
  description = "Cluster Client CA Certificate"
}

output "cluster_kube_config_raw" {
  value = azurerm_kubernetes_cluster.k8s.kube_config_raw
  description = "Raw Kube Config"
}
