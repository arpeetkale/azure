# TODO: Similar suffix variable in other files
locals {
  aks_cluster_suffix = "AKS-CLUSTER"
}

# Create K8's cluster
resource "azurerm_kubernetes_cluster" "k8s" {
    name                = "${var.infra_prefix}-${var.environment}-${local.aks_cluster_suffix}"
    location            = var.location
    resource_group_name = var.resource_group_name
    dns_prefix          = "${var.infra_prefix}-${var.environment}-${local.aks_cluster_suffix}"

    linux_profile {
        admin_username = var.admin_user_for_cluster_vm

        ssh_key {
            key_data = file(var.cluster_vm_ssh_key)
        }
    }

    default_node_pool {
        name            = lower("${var.infra_prefix}${var.environment}POOL")
        node_count      = var.cluster_node_pool_size
        vm_size         = var.cluster_node_vm_size
    }
    
    identity {
        type = "SystemAssigned"
    }

    tags = var.tags
}
