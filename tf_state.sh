#!/bin/bash

set -x

LOCATION=westus2
CONTAINER_NAME=tfstate
RESOURCE_GROUP_NAME=tfstate
STORAGE_ACCOUNT_NAME=tfstate$RANDOM

# Create resource group
az group create \
--name $RESOURCE_GROUP_NAME \
--location $LOCATION

# Create storage account
az storage account create \
--resource-group $RESOURCE_GROUP_NAME \
--name $STORAGE_ACCOUNT_NAME \
--sku Standard_LRS \
--encryption-services blob

# Get storage account key
ACCOUNT_KEY=$( \
    az storage account keys list \
    --resource-group $RESOURCE_GROUP_NAME \
    --account-name $STORAGE_ACCOUNT_NAME \
    --query "[0].value" -o tsv)

# Create blob container
az storage container create \
--name $CONTAINER_NAME \
--account-name $STORAGE_ACCOUNT_NAME \
--account-key $ACCOUNT_KEY