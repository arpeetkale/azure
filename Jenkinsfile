pipeline {
  environment {
    image_name = "azure/sample-python-app"
    registry_credential = "azure-container-registry-creds"
    registry_url = "http://infradevregistry.azurecr.io"
    registry_name = "infradevregistry.azurecr.io"
    docker_image = ""
  }
  agent any
  stages {
    stage('Building image') {
      steps{
        script {
          docker_image = docker.build(image_name, "$WORKSPACE/sample-python-app")
        }
      }
    }
    stage('Deploy Image') {
      steps{
        script {
          docker.withRegistry(registry_url, registry_credential) {
            docker_image.push("$BUILD_NUMBER")
            docker_image.push('latest')
          }
        }
      }
    }
    stage('Remove Unused docker image') {
      steps{
        sh "docker rmi $registry_name/$image_name:$BUILD_NUMBER"
        sh "docker rmi $registry_name/$image_name:latest"
      }
    }
    stage('Package Helm chart') {
      steps{
        sh "helm package $WORKSPACE/sample-python-app/helm/sample-python-app"
      }
    }
    stage('Deploy to AKS Cluster') {
      steps{
        withCredentials([file(credentialsId: 'aks-cluster-kube-config', variable: 'config')]) {
          sh """
          export KUBECONFIG=\${config}
          helm upgrade sample-python-app $WORKSPACE/sample-python-app/helm/sample-python-app
          """
        }
      }
    }
  }
}
