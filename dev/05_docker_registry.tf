# TODO: This file could be abstracted as a module to create ACR registry & a service principal to access/use it.

# Create a new container registry - ACR
module "docker_registry" {
  count                    = (var.create_docker_registry == true ? 1 : 0)
  source                   = "../modules/infra/docker_registry"
  resource_group_name      = "${module.network.resource_group_name}"
  location                 = "${module.network.location}"
  infra_prefix             = "${module.network.infra_prefix}"
  environment              = "${module.network.environment}"
  tags                     = var.tags
}

# Create application dedicated as a ACR service account
resource "azuread_application" "acr-app" {
  count = (var.create_docker_registry == true ? 1 : 0)
  name  = "${var.infra_prefix}-${var.environment}-ACR-APP"
}

# Create a service principal
resource "azuread_service_principal" "acr-sp" {
  count          = (var.create_docker_registry == true ? 1 : 0)
  application_id = "${azuread_application.acr-app[0].application_id}"
}

# Random generated password
resource "random_password" "password" {
  count = (var.create_docker_registry == true ? 1 : 0)
  length = 16
  special = true
  override_special = "_%@"
}

# Create password for service principal
resource "azuread_service_principal_password" "acr-sp-pass" {
  count = (var.create_docker_registry == true ? 1 : 0)
  service_principal_id = "${azuread_service_principal.acr-sp[0].id}"
  value                = random_password.password[0].result
  end_date             = timeadd(timestamp(), "8760h")
}

# Assign Contributor role to this service principal - to be used with Jenkins
resource "azurerm_role_assignment" "acr-assignment" {
  count = (var.create_docker_registry == true ? 1 : 0)
  scope                = "${module.docker_registry[0].acr_id}"
  role_definition_name = "Contributor"
  principal_id         = "${azuread_service_principal_password.acr-sp-pass[0].service_principal_id}"
}

# Output docker login credentials for this service principal
output "docker_login_server" {
  value = "${var.create_docker_registry == true ? module.docker_registry[0].acr_login_server : ""}"
  description = "Login server URL of the ACR"
}
    
output "docker_service_principal_uid" {
  value = "${var.create_docker_registry == true ? azuread_service_principal.acr-sp[0].application_id : ""}"
  description = "Id of the service principal for ACR"
}

output "docker_service_principal_password" {
  value = "${var.create_docker_registry == true ? azuread_service_principal_password.acr-sp-pass[0].value : ""}"
  description = "Password of the service principal for ACR"
}
