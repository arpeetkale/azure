locals {
  jenkins_resource_name_prefix         = "${module.network.infra_prefix}-${module.network.environment}-JENKINS"
  jenkins_vm_nic_ip_config_name        = "${local.jenkins_resource_name_prefix}-NIC-IP-CFG"
  jenkins_lb_frontend_ip_config_name   = "${local.jenkins_resource_name_prefix}-LB-NIC-IP-CFG"
}

module "jenkins" {
  source                                  = "../modules/infra/linux_vm"
  resource_group_name                     = "${module.network.resource_group_name}"
  vnet_name                               = "${module.network.virtual_network_name}"
  subnet_name                             = "${module.network.private_subnet_1_name}"
  tags                                    = var.tags
  linux_vm_name                           = local.jenkins_resource_name_prefix
  linux_vm_create_public_ip               = false
  linux_vm_extension_script_file          = abspath("${path.module}/../files/jenkins.sh")
  linux_vm_network_security_group_id      = "${module.security_group.jenkins_network_security_group_id}"
  linux_vm_application_security_group_id  = "${module.security_group.bastion_application_security_group_id}"
  depends_on                              = [module.network, module.security_group]
} 

# Create Public IP for Jenkins LB
resource "azurerm_public_ip" "jenkins_lb_public_ip" {
  name                = "${local.jenkins_resource_name_prefix}-LB-PIP"
  location            = "${module.network.location}"
  resource_group_name = "${module.network.resource_group_name}"
  allocation_method   = "Static"
  sku                 = "Standard"
  domain_name_label   = var.jenkins_dns_label
  tags                = var.tags
  depends_on          = [module.jenkins]
}

# Create Jenkins LB
resource "azurerm_lb" "jenkins_lb" {
  name                          = "${local.jenkins_resource_name_prefix}-LB"
  location                      = "${module.network.location}"
  resource_group_name           = "${module.network.resource_group_name}"
  sku                           = "Standard"
  frontend_ip_configuration {
    name                        = local.jenkins_lb_frontend_ip_config_name
    public_ip_address_id        = azurerm_public_ip.jenkins_lb_public_ip.id
  }
  depends_on                    = [azurerm_public_ip.jenkins_lb_public_ip]
}

# Create Backend Address Pool for Jenkins LB
resource "azurerm_lb_backend_address_pool" "jenkins_lb_backend_pool" {
  name                = "${local.jenkins_resource_name_prefix}-BE-POOL"
  resource_group_name = "${module.network.resource_group_name}"
  loadbalancer_id     = azurerm_lb.jenkins_lb.id
  depends_on          = [ azurerm_lb.jenkins_lb ]
}

# Associate Jenkins LB & Jenkins NIC for Backend Address Pool
resource "azurerm_network_interface_backend_address_pool_association" "jenkins_vm_nic_and_backend_pool_association" {
  backend_address_pool_id = azurerm_lb_backend_address_pool.jenkins_lb_backend_pool.id
  ip_configuration_name   = local.jenkins_vm_nic_ip_config_name
  network_interface_id    = "${module.jenkins.linux_vm_network_interface_id}"
  depends_on              = [azurerm_lb_backend_address_pool.jenkins_lb_backend_pool]
}

# Create Jenkins LB NAT Rule
resource "azurerm_lb_nat_rule" "jenkins_lb_nat_rule" {
  name                           = "${local.jenkins_resource_name_prefix}-LB-RULE"
  resource_group_name            = "${module.network.resource_group_name}"
  loadbalancer_id                = azurerm_lb.jenkins_lb.id
  protocol                       = "Tcp"
  frontend_port                  = "80"
  backend_port                   = "8080"
  idle_timeout_in_minutes        = 5
  frontend_ip_configuration_name = local.jenkins_lb_frontend_ip_config_name
  depends_on                     = [azurerm_network_interface_backend_address_pool_association.jenkins_vm_nic_and_backend_pool_association]
}

# Associate Jenkins LB NAT Rule to Jenkins VM NIC
resource "azurerm_network_interface_nat_rule_association" "natrule" {
  nat_rule_id           = azurerm_lb_nat_rule.jenkins_lb_nat_rule.id
  ip_configuration_name = local.jenkins_vm_nic_ip_config_name
  network_interface_id  = "${module.jenkins.linux_vm_network_interface_id}"
}

# Create Jenkins LB Probe to check health of Jenkins process on Jenkins VM
resource "azurerm_lb_probe" "jenkins_lb_backend_probe" {
  name                = "${local.jenkins_resource_name_prefix}-BE-PROBE"
  resource_group_name = "${module.network.resource_group_name}"
  port                = "8080"
  protocol            = "Http"
  request_path        = "/"
  loadbalancer_id     = azurerm_lb.jenkins_lb.id
  depends_on          = [azurerm_lb.jenkins_lb]
}

# Added outbound rule for Jenkins VM behind LB
resource "azurerm_lb_outbound_rule" "jenkins_lb_outbound_rule" {
  resource_group_name     = "${module.network.resource_group_name}"
  loadbalancer_id         = azurerm_lb.jenkins_lb.id
  name                    = "${local.jenkins_resource_name_prefix}-OUTBOUND-RULE"
  protocol                = "All"
  backend_address_pool_id = azurerm_lb_backend_address_pool.jenkins_lb_backend_pool.id
  frontend_ip_configuration {
    name = local.jenkins_lb_frontend_ip_config_name
  }
}
