module "security_group" {
  source                 = "../modules/infra/security_group"
  infra_prefix           = "${module.network.infra_prefix}"
  environment            = "${module.network.environment}"
  resource_group_name    = "${module.network.resource_group_name}"
  tags                   = var.tags
  depends_on             = [module.network]
}

