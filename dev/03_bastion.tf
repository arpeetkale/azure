module "bastion" {
  source                                  = "../modules/infra/linux_vm"
  resource_group_name                     = "${module.network.resource_group_name}"
  vnet_name                               = "${module.network.virtual_network_name}"
  subnet_name                             = "${module.network.public_subnet_1_name}"
  linux_vm_name                           = "${module.network.infra_prefix}-${module.network.environment}-BASTION"
  tags                                    = var.tags
  linux_vm_create_public_ip               = true
  linux_vm_network_security_group_id      = "${module.security_group.bastion_network_security_group_id}"
  linux_vm_application_security_group_id  = "${module.security_group.bastion_application_security_group_id}"
  depends_on                              = [module.network, module.security_group]
}
