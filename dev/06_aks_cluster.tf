module "aks_cluster" {
    source                 = "../modules/infra/aks_cluster"
    resource_group_name    = "${module.network.resource_group_name}"
    location               = "${module.network.location}"
    infra_prefix           = var.infra_prefix
    environment            = var.environment
    tags                   = var.tags
}

output "cluster_id" {
  value = "${module.aks_cluster.cluster_id}"
  description = "Cluster ID"
}

output "cluster_fqdn" {
  value = "${module.aks_cluster.cluster_fqdn}"
  description = "Cluster FQDN"
}

output "cluster_name" {
  value = "${module.aks_cluster.cluster_name}"
  description = "Name of K8's cluster"
}

output "cluster_node_pool_name" {
  value = "${module.aks_cluster.cluster_node_pool_name}"
  description = "Name of K8's cluster"
}

output "cluster_node_resource_group" {
  value = "${module.aks_cluster.cluster_node_resource_group}"
  description = "Cluster Node Resource Group"
}

output "cluster_client_key" {
  value = "${module.aks_cluster.cluster_client_key}"
  description = "Cluster Client Key"
}

output "cluster_client_certificate" {
  value = "${module.aks_cluster.cluster_client_certificate}"
  description = "Cluster Client Certificate"
}

output "cluster_ca_certificate" {
  value = "${module.aks_cluster.cluster_ca_certificate}"
  description = "Cluster Client CA Certificate"
}

output "cluster_kube_config_raw" {
  value = "${module.aks_cluster.cluster_kube_config_raw}"
  description = "Cluster Kube Config Raw"
}
