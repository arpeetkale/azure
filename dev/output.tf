output "virtual_network_name" {
  value       = module.network.virtual_network_name
  description = "Name of the Virtual Network created"
}

output "public_subnet_1_name" {
  value       = module.network.public_subnet_1_name
  description = "Name of the public subnet 1"
}

output "private_subnet_1_name" {
  value       = module.network.private_subnet_1_name
  description = "Name of the private subnet 1"
}
