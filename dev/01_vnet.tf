# Configure the Azure provider
terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = ">= 2.26"
    }
    azuread = {
      source = "hashicorp/azuread"
      version = ">= 1.1.1"
    }
  }
  backend "azurerm" {
    resource_group_name   = "tfstate"
    storage_account_name  = "tfstate10400"
    container_name        = "tfstate"
    key                   = "terraform.tfstate"
  }
}

provider "azurerm" {
  features {}
}

provider "azuread" {}

module "network" {
  source                 = "../modules/infra/network"
  location               = "westus2"
  infra_prefix           = var.infra_prefix
  environment            = var.environment
  vnet_cidr              = "10.0.0.0/16"
  public_subnet_1_cidr   = "10.0.1.0/24"
  private_subnet_1_cidr  = "10.0.2.0/24"
  tags                   = var.tags
}
