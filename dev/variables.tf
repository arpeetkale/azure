variable "tags" {
  type = map
  default = {
    Environment = "DEV"
    Owner       = "Devops"
  }
}

variable "infra_prefix" {
  type    = string
  default = "INFRA"
  description = "Infrastructure related resources will be prefixed by INFRA string."
}

variable "app_prefix" {
  type    = string
  default = "APP"
  description = "Application related resources will be prefixed by APP string."
}

variable "environment" {
  type    = string
  default = "DEV"
  validation {
    condition     = length(var.environment) <= 4
    error_message = "Max number of characters allowed for variable environment is 4."
  }
}

variable "create_docker_registry" {
  type        = bool
  description = "Choice to create docker registry while setting up infra resources"
  default     = false
}

variable "jenkins_dns_label" {
  type        = string
  description = "DNS label for Jenkins instance"
}
